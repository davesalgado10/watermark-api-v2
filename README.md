# Rest API using Scala and Play
A Rest API based on Lightbend's example project for [Making a REST API in Play](http://developer.lightbend.com/guides/play-rest-api/index.html).

## Running

You need to download and install sbt for this application to run.

Once you have sbt installed, the following at the command prompt will start up Play in development mode:

```
sbt run
```

Play will start up on the HTTP port at http://localhost:9000/.   You don't need to reploy or reload anything -- changing any source code while the server is running will automatically recompile and hot-reload the application on the next HTTP request.

## Testing

To run unit tests:
```
sbt test
```

You can also perform tests by using any rest client. Url for watermarkservice API:
```
 http://localhost:9000/watermarkservice
```

## Features
### Create watermark
Create a watermark for Document, returns a ticket for polling of result

* URL - http://localhost:9000/watermarkservice/create
* METHOD - POST
* Json Body - {title:String, author:String, topic:String} where topic is optional

Sample request:
```
curl -H "Content-Type: application/json" -X POST http://localhost:9000/watermarkservice/create -d '{"title":"The Dark Code", "author":"Bruce Wayne", "topic":"Science"}'
```
Sample response:

```
#!json

{"ticket": 1}
```
Custom Error: Missing Json Body

```
#!json
{"status":"nok","message":"missing watermark json"}

```



### Retrieve document by ticket
Retrieve document with watermark by ticket, returns a Json representation of the document 

* URL - http://localhost:9000/watermarkservice/$ticket
* METHOD - GET
* URL PARAM - $ticket: Long

Sample Request
```
curl -H "Accept: application/json" -X GET http://localhost:9000/watermarkservice/1
```
Sample Response

```
#!json

{"title":"The Dark Code","author":"Bruce Wayne","topic":"Science","watermark":{"content":"book","title":"The Dark Code","author":"Bruce Wayne","topic":"Science"}}
```
Custom Error: Ticket is not a number

```
#!json
{"status":"nok","error":{"message":"invalid ticket - not a number"}}

```